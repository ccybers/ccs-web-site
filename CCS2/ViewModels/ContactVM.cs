﻿using System.ComponentModel.DataAnnotations;

namespace CCS2.ViewModels
{
    public class ContactVM
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Telephone { get; set; }

        [Required]
        public string Message { get; set; }
    }
}