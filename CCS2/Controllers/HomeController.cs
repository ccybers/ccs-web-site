﻿using CCS2.Models;
using CCS2.ViewModels;
using System;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace CCS2.Controllers
{
    public class HomeController : Controller
    {
        //SMTP hosting server address
        const string SMTP_HOST = "mail.creativecybersolutions.net";
        //TODO: enter credentials
        const string SMTP_ACCOUNT = "contact@creativecybersolutions.net";
        const string SMTP_PASSWORD = "**********";

        CcsContext _db = new CcsContext();

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Windows, Web, and Mobile Software Development.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Creative CyberSolutions Today";

            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactVM e)
        {
            if (ModelState.IsValid)
            {
                //message
                StringBuilder message = new StringBuilder();
                message.Append("Name: " + e.Name + "\n");
                message.Append("Email: " + e.Email + "\n");
                message.Append("Telephone: " + e.Telephone + "\n\n");
                message.Append(e.Message);

                try
                {
                    SmtpClient smtpClient = new SmtpClient(SMTP_HOST, 25);

                    smtpClient.Credentials = new System.Net.NetworkCredential(SMTP_ACCOUNT, SMTP_PASSWORD);
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = true;
                    MailMessage mail = new MailMessage();

                    //Setting From , To and CC
                    mail.From = new MailAddress(e.Email, e.Name);
                    mail.To.Add(new MailAddress(SMTP_ACCOUNT));

                    //subject
                    mail.Subject = "Message From " + e.Name;
                    //body
                    mail.Body = message.ToString();

                    smtpClient.Send(mail);
                }
                catch (Exception ex)
                {
                    
                    string fixedMessage = message.ToString();
                    System.Diagnostics.Debug.WriteLine("Pre-conversion: " + fixedMessage);
                    fixedMessage = Regex.Replace(fixedMessage, @"\r\n?|\n", "%0A");
                    System.Diagnostics.Debug.WriteLine("Post-conversion: " + fixedMessage);
                    Response.Redirect("mailto:" + SMTP_ACCOUNT 
                                    + "?subject=" + "Message From " + e.Name
                                    + "&body=" + fixedMessage);
                }
            }
            return View();
        }
    }
}
